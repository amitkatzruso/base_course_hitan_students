package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectionsController {
    @Autowired
    CrudDataBase dataBase;

    @Autowired
    AirplanesAllocationManager AAManager;

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getEjectedPilotsInfo() {
        return dataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("/takeResponsibility")
    @ResponseBody
    public void takeResponsibility(@RequestParam int ejectionId, @CookieValue(name = "client-id") String clientId) {
        EjectedPilotInfo ejectedPilot = dataBase.getByID(ejectionId, EjectedPilotInfo.class);

        if (ejectedPilot.getRescuedBy() == null) {
            ejectedPilot.setRescuedBy(clientId);
            dataBase.update(ejectedPilot);
            AAManager.allocateAirplanesForEjection(ejectedPilot, clientId);
        }
    }
}
