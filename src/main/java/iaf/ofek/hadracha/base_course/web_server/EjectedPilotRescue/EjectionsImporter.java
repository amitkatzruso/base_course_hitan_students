package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;
    @Value("${ejections.namespace}")
    public String NAMESPACE;

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final RestTemplate restTemplate;
    private final CrudDataBase dataBase;
    private final ListOperations listOperations;
    private static final Double SHIFT_NORTH = 1.7;

    public EjectionsImporter(RestTemplateBuilder restTemplateBuilder, CrudDataBase dataBase, ListOperations listOperations) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperations = listOperations;
        executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);

    }

    private List<EjectedPilotInfo> getEjectedPilotInfoFromServer(){
        try {
            List<EjectedPilotInfo> ejectionsFromServer;
            ResponseEntity<List<EjectedPilotInfo>> responseEntity = restTemplate.exchange(
                    EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                    null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                    });
            ejectionsFromServer = responseEntity.getBody();
            if (ejectionsFromServer != null) {
                for(EjectedPilotInfo ejectedPilotInfo: ejectionsFromServer) {

                    Coordinates newCoordinates = ejectedPilotInfo.getCoordinates();
                    newCoordinates.lat += SHIFT_NORTH;
                    ejectedPilotInfo.setCoordinates(newCoordinates);
                }
            }
            return ejectionsFromServer;
        }
        catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    private void addEjectionsToDB(List<EjectedPilotInfo> updatedEjections,List<EjectedPilotInfo> previousEjections){
        try
        {
            List<EjectedPilotInfo> addedEjections = listOperations.subtract(updatedEjections, previousEjections, new Entity.ByIdEqualizer<>());
            addedEjections.forEach(dataBase::create);
        }
        catch (RestClientException e)
        {
            System.err.println("Could not create ejections: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void removeEjectionsFromDB(List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections){
        try {
            List<EjectedPilotInfo> removedEjections = listOperations.subtract(previousEjections, updatedEjections, new Entity.ByIdEqualizer<>());
            removedEjections.stream().map(EjectedPilotInfo::getId).forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
        }
        catch (RestClientException e)
        {
            System.err.println("Could not remove ejections: " + e.getMessage());
            e.printStackTrace();
        }

    }

    private void updateEjections() {
        try {
            List<EjectedPilotInfo> ejectionsFromServer=getEjectedPilotInfoFromServer();
            addEjectionsToDB(ejectionsFromServer,dataBase.getAllOfType(EjectedPilotInfo.class));
            removeEjectionsFromDB(ejectionsFromServer,dataBase.getAllOfType(EjectedPilotInfo.class));
            }
        catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
        }
    }


}
